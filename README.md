INTRODUCTION
------------

Offers a simple solution to generate electronic documents: json + docx => PDF/DOCX

REQUIREMENTS
------------

> Drupal8, 

> LibreOffice >= 7.2.0.4, 

> phpWord >= v0.18, 

> PHP7.4

INSTALLATION
------------
To install the module, run:

  composer require drupal/editics

To active the module, run:

  drush en editics

To access the demo page: configuration > Web service W IRC - Demo space (url: /demo/form)

WARNING:
There is a bug when you click the [run] button.
1. Click the [run] button
2. Download the template [docx]
3. Reload it
4. Then re-run


CONFIGURATION
-------------

It is possible to configure remote accesses if you want to use the API (not yet available).

Configuration page: /admin/api/configuration.

USAGE
=====

Simply use the service :

    $response = Drupal::service('default.rest.editics.service')->_call(JSONDATA, OPTIONS).

JSONDATA : Flux Json

JSON structure:

    {
        "call": {
            "action": "",
            "download": true,
            "models": [
                {
                    "docKey": "",
                    "format": "pdf",
                    "convertor": "soffice",
                    "contentFile": "",
                    "settings": {
                    "data": [
                        {
                            "id": "",
                            "type": "",
                            "value": ""
                        },
                        {
                            "id": "",
                            "type": "",
                            "value": {
                                "val": ""
                                "styles": {
                                    "Color": "#5dade2"
                                }
                            }
                        }
                        ]
                    }
                }
            ]
        }
    }

    "action"      : The action associated with the call. It takes one of the following values: editique, convert
    "download"    : defines the format of the response. If this field is TRUE, the response of the call returns
                    a url to retrieve the generated document. Otherwise, the document is returned to base64 format.
    "docKey"      : specifies the key that will allow you to retrieve the document in the response object. 
                    Because it is possible to make the call with plusierus documents.
                    Then to distinguish them, each document must bear a key.
    "format"      : specifies the output format of the document. Currently, there are only two possible formats: pdf, docx.
    "convertor"   : allows you to specify the converter you want to use (for a PDF output). Currently possible values: soffice
    "contentFile" : this field is used to transmit the model document in base64.
    "settings"    : this block allows to transmit the data that will ventilate the model.
    "id"          : this is the keyword contained in the template and will need to be replaced by the transmitted value. 
                    ex. if in the model we have the keyword ${firstName}, then the field [id] will take [firstName]: "id": "firstName"
    "type"        : The type of the value in the [value] field. This field can have several values: text, evaluate, checkbox,
                    table, liste, image, listpuce, listcustompuce, checkboxlist.
    "value"       : the value to be transmitted to the model.
    "val"         : the value to be transmitted to the model.
    "styles"      : allows to transmit formatting styles for the transmitted value. 
                    For details on how to use styles, see the example json and API documentation.

    NB: Depending on the type of the value, the structure of the [value] field may be different. see the example json.


    OPTIONS : [
      '_remote' => TRUE/FALSE
      '_env' => 'test/prod/production'
    ]

    '_remote'  : allows you to specify if you want to attack the remote server for editing.
                 
                 If _remote = TRUE, the module will call the remote API to generate the documents,
                 this method is more efficient because it detaches the host application from document processing:
                 faster generation because benefits from the performance of the remote server (which only does that.)

                 If _remote = FALSE, document processing is done locally: slower or depending on the performance of the host server.
                 As the processing server is not currently available, this value must be set to FALSE.

    '_env'    : in case of remote server solicitation, this option allows to specify if it is the recipe server
                or production that we want to attack.

                If _env = test, the authentication data will be more from the configuration page:  blocks recipe.
                If _env = 'prod' or 'production', the data from the production block will be recovered for authentication.

                NB:
                To have this authentication information, you must create an account on the server (not yet available).


A demo tool accompanies the api. To activate it: 

    drush en cri_demo

This activates the entire api. If you don’t want to enable the demo module, just do 

    drush en cri_editics

To access the demo page: configuration>Web services > IRC - Demo space (url: /demo/form)

DOCUMENTATION
-------------

A user guide is available in the demo space.

full documentation is available here : http://mydoc.api-cri.fr/documentations

> The English version is currently being drafted.

TRANSLATION
-----------

You will find at the root of the module the translation file of the module in French: editics-1-0-x-fr.po

MAINTAINERS
-----------

 - MOHAMED A. Idjabou (Idjabou) - https://www.drupal.org/u/idjabou