<?php

namespace Drupal\cri_core_mapping\Process;

use Drupal;
use Drupal\node\Entity\Node;

class Process {

  /**
   * @param $entity
   * @param $settings
   * @return array
   */
  public static function logo($entity, $settings) {
    $logo = 'sites/default/files/images/logo/logobleu.png';
    if (file_exists($logo)) {
      return ['extension' => 'png', 'content' => base64_encode(file_get_contents($logo))];
    }
  }


  /**
   * @param $entity
   * @param $settings
   * @return string
   */
  public static function loginUri($entity, $settings) {
    global $base_url;
    return $base_url;
  }

  /**
   * @param $entity
   * @param $settings
   * @return string
   */
  public static function loginId($entity, $settings) {
    // dump($entity->get('name')->getString());
    return $entity->get('name')->getString();
  }
}
