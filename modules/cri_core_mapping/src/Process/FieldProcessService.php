<?php

namespace Drupal\cri_core_mapping\Process;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\node\Entity\Node;
use Drupal\user\Entity\User;
use Drupal\Core\Entity\ContentEntityBase;

class FieldProcessService {
  /**
   * @var User
   */
  public $_currentUser;

  public function __construct(AccountProxyInterface $account) {
    $this->_currentUser = User::load($account->id());
  }
  /**
   * Generic Process function
   */
  public function process($mappingField, $entity, $settings) {
    $value = '';
    if (!empty($settings['id'])) {
      if ($entity instanceof ContentEntityBase && $entity->hasField($settings['id'])) {
        $value = $entity->get($settings['id'])->getValue();
      }elseif (is_array($entity)) {
        $fields = explode('/', $settings['id']);
        $value = NestedArray::getValue($entity, $fields);
      }
    } elseif(isset($settings['value'])) {
      $value = $settings['value'];
    }
    $className = '';
    switch ($settings['type']) {
      case 'custom':
        if (method_exists($settings['process']['class'], $settings['process']['method'])) {
          $processClassName = $settings['process']['class'];
          $method = $settings['process']['method'];
          if (!empty($settings['process']['mode']) && $settings['process']['mode'] === 'object') {
            $process = new $processClassName();
            $value = $process->$method($entity, $settings);
          }else {
            $value = $processClassName::$method($entity, $settings);
          }

          $settings['type'] = $settings['process']['type'];
          $className = 'Drupal\cri_core_mapping\Models\Types\\' . ucfirst($settings['process']['type']);
        }
        break;
      default:
        $className = 'Drupal\cri_core_mapping\Models\Types\\' . ucfirst($settings['type']);
        break;
    }

    if (class_exists($className)) {
      $this->specialcharCleaner($value);
      return $className::format($mappingField, $value, $settings);
    }
    return $value;
  }
  /**
   * Clean special char
   * 
   * @return @value
   */
  protected function specialcharCleaner(&$value) {
    if (is_string($value)) {
      $value = str_ireplace(['&'], ['&amp;'], $value);
    }elseif(is_array($value)) {
      foreach($value as &$v) {
        $this->specialcharCleaner($v);
      }
    }
  }
}
