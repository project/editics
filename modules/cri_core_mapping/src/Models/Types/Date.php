<?php

namespace Drupal\cri_core_mapping\Models\Types;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;

class Date extends Type {
  /**
   * @param $value
   * @param $settings
   * @return array
   */
  public static function format($mappigField, $value, $settings) {
    $data = parent::format($mappigField, $value, $settings);
    if (is_array($data['value'])) {
      $data['value'] = !empty($data['value'][0]) ? $data['value'][0]['value'] : $data['value'];
      if ($data['value'] instanceof DrupalDateTime) {
        $data['value'] = $data['value']->format('d/m/Y');
      }else {
        $date = new DrupalDateTime($data['value']);
        $data['value'] = $date->format('d/m/Y');
      }
    }elseif($data['value'] instanceof DrupalDateTime) {
      $data['value'] = $data['value']->format('d/m/Y');
    }

    return $data;
  }
}
