<?php

namespace Drupal\cri_core_mapping\Models\Types;

class Checkboxlist extends Type {
  /**
   * @param $value
   * @param $settings
   * @return array
   */
  public static function format($mappigField, $value, $settings) {
    $data = parent::format($mappigField, $value, $settings);
    if (is_array($data['value'])) {
      foreach($data['value'] as &$v) {
        $v = ['val' => $v['value'], 'checked' => !empty($v['checked']) ?: false ];
      }
    }

    return $data;
  }
}
