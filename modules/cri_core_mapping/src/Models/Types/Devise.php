<?php

namespace Drupal\cri_core_mapping\Models\Types;

class Devise extends Type {
  /**
   * @param $value
   * @param $settings
   * @return array
   */
  public static function format($mappigField, $value, $settings) {
    $data = parent::format($mappigField, $value, $settings);

    if (is_array($data['value'])) {
      $data['value'] = !empty($data['value'][0]) ? $data['value'][0]['value'] : $data['value'];
      $data['value'] = number_format($data['value'], 2, ',', ' ') . '€';
    }else {
      $data['value'] = !empty($data['value']) ? number_format($data['value'], 2, ',', ' ') . '€' : '--';
    }
    return $data;
  }
}
