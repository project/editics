<?php

namespace Drupal\cri_core_mapping\Models\Types;

class Text extends Type {
  /**
   * @param $value
   * @param $settings
   * @return array
   */
  public static function format($mappigField, $value, $settings) {
    $data = parent::format($mappigField, $value, $settings);
    // dump(array($mappigField, $data));
    if (is_array($data['value'])) {
      $data['value'] = !empty($data['value'][0]) ? $data['value'][0]['value'] : $data['value'];
    }
    return $data;
  }
}
