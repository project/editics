<?php

namespace Drupal\cri_core_mapping\Models\Types;

class Image extends Type {
  /**
   * @param $value
   * @param $settings
   * @return array
   */
  public static function format($mappigField, $value, $settings) {
    $data = parent::format($mappigField, $value, $settings);
    if (!empty($data['value']['extension'])) {
      $data['extension'] = $data['value']['extension'];
      $data['content'] = $data['value']['content'];
      unset($data['value']);
    }
    return $data;
  }
}
