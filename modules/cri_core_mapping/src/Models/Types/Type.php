<?php

namespace Drupal\cri_core_mapping\Models\Types;

class Type implements TypeInterface {
  /**
   * @param $mappingField
   * @param $value
   * @param $settings
   * @return array
   */
  public static function format($mappingField, $value, $settings) {
    return ['id' => $mappingField, 'type' => $settings['type'], 'value' => $value];
  }
  /**
   * Parcours un tableau pour donner la valeur qui se trouve au bout
   * 
   * @param mixed $value
   * @return string $value
   */
  public static function getValueFromArray($value) {
    if (is_array($value)) {
      foreach($value as $v) {
        return self::getValueFromArray($v);
      }
    }
    return $value;
  }
}
