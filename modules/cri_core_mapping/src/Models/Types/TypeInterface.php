<?php

namespace Drupal\cri_core_mapping\Models\Types;

interface TypeInterface {
  public static function format($mappingField, $value, $settings);
}
