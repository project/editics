<?php

namespace Drupal\cri_core_mapping\Service;

use Drupal;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\user\Entity\User;
use Symfony\Component\Yaml\Yaml;

/**
 * Class MappingService
 * @package Drupal\cri_core_mapping\Service
 */
class MappingService {
  /**
   * @var
   */
  private $_currentUser;
  private $_models = [];

  /**
   * MappingService constructor.
   * @param AccountProxyInterface $accountProxy
   */
  public function __construct(AccountProxyInterface $accountProxy) {
    $this->_currentUser = User::load($accountProxy->id());
  }

  /**
   * @param string $mappingFile
   * @param array $entities
   * @return false|string
   */
  public function map(string $mappingFile, array $entities, $dockey, $format, $convertor, $contentFile) {
    $model = [
      'docKey' => $dockey,
      'format' => $format,
      'convertor' => $convertor,
      'contentFile' => $contentFile,
      'settings' => [
        'data' => []
      ]
    ];

    if (file_exists($mappingFile)) {
      $maps = (array) Yaml::parse(file_get_contents($mappingFile));
      if (is_array($maps)) {
        $fieldService = Drupal::service('cri_core_mapping.field.service');
        foreach ($maps as $entityType => $map) {
          if (!empty($entities[$entityType])) {
            if (!empty($map['variables'])) {
              foreach ($map['variables'] as $field => $settings) {
                $settings['docType'] = $dockey;
                $field = $fieldService->process($field, $entities[$entityType], $settings);
                if (!empty($field)) {
                  $model['settings']['data'][] = $field;
                }
              }
            }
          }
        }
      }
    }
    $this->_models[] = $model;
    return $this->_models;
  }

  /**
   * @param $action
   * @return array[]
   */
  public function format($action) {
    return [
      'call' => [
        'action' => $action,
        'models' => $this->_models
      ]
    ];
  }

  /**
   * @param $object
   * @param $objectData
   */
  public function serializeFromClass($object, &$objectData) {
    $reflectionClass = new \ReflectionObject($object);

    $properties = $reflectionClass->getProperties();
    foreach ($properties as $property) {
      preg_match_all('/@var\s+([^\s]+)/m', $property->getDocComment(), $matches);
      $name = $property->getName();

      $value = property_exists($object, $name) ? $object->$name : NULL;
      if ($property->isPublic() && !empty($matches[1])) {
        $type = $matches[1][0];
        switch ($type) {
          case 'string':
          case 'bool':
          case 'array':
            $objectData->$name = $value;
            break;
          default:
            try {
              if (is_array($value) && empty($value)) {
                $objectData->$name = [];
                $value = [new $type];
              }
              if (gettype($object->$name) == 'array' || gettype($objectData->$name) == 'array') {
                if (!is_array($value)) {
                  $value = [$value];
                }
                foreach ($value as $val) {
                  $nObject = new $type();
                  $this->serializeFromClass($nObject, $val);
                  $objectData->$name[] = $val;
                }
              }else {
                if (!is_object($value)) {
                  $value = $nObject = new $type();
                  $this->serializeFromClass($nObject, $value);
                }
                $objectData->$name = $value;
              }
            } catch (\Exception $exc) {
              $objectData->$name = $value;
            }
            break;
        }
      }
    }
  }

  /**
   * @param $endpoint
   * @return \stdClass[]|void[]
   */
  public function getSkeletoncontent($endpoint) {
    $dataResult = new \stdClass();
    if (preg_match('/Response/i', $endpoint)) {
      $className = '\Drupal\cri_demo\models\ResponseModel';
    }else {
      $className = '\Drupal\cri_demo\models\RequestModel';
    }
    if (class_exists($className)) {
      $class = new $className();
      $this->serializeFromClass($class, $dataResult);
    }
    return ['call' => $dataResult];
  }
}
