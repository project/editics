<?php

namespace Drupal\cri_php_word\resources;

use Drupal\cri_php_word\resources\styles\themes\DefaultStyleBlueTheme;
use Drupal\cri_php_word\resources\styles\themes\DefaultStyleGreyTheme;
use Drupal\cri_php_word\resources\styles\themes\DefaultStyleTheme;
use Drupal\cri_php_word\resources\styles\DefaultStyleTableInterface;

class ResourcesManager {

  public static function getResource(string $resourceKey) : DefaultStyleTableInterface {
    switch ($resourceKey) {
      case 'DEFAULT_THEME':
        return new DefaultStyleTheme();
      case 'DEFAULT_BLUE_THEME':
        return new DefaultStyleBlueTheme();
      case 'DEFAULT_GREY_THEME':
        return new DefaultStyleGreyTheme();
        break;
      default:
        break;
    }
  }
}
