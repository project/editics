<?php
namespace Drupal\cri_php_word\resources\styles\themes;

use Drupal\cri_php_word\resources\styles\DefaultStyleTableInterface;

class DefaultStyleTheme implements DefaultStyleTableInterface {
  const TABLE_TITLE_STYLES = [
    'cell' => [
      'bgColor' => '#ffffff',
      'borderColor' => '#ffffff',
      'borderSize' => 1
    ],
    'text' => [
      'font' => [
        'bold' => true,
        'size' => 12,
        'underline' => 'single'
      ],
      'position' => [
        'align' => 'left'
      ]
    ]
  ];
  const TABLE_STYLES = [
    'table' => [
      'borderSize' => 0,
      'bgColor' => '#ffffff'
    ],
    'cell' => [
      'bgColor' => '#ffffff',
      "valign" => "center",
      "borderSize" => "3",
      "borderColor" => "#cccccc"
    ],
    'text' => [
      'font' => [
        'bold' => 'false',
        "color" => "#566573"
      ],
      'position' => [
        'align' => 'center',
        'spaceBefore' => 50,
        'spaceAfter' => 50
      ]
    ]
  ];
  const TABLE_HEAD_STYLES = [
    'cell' => [
      'bgColor' => '#eeeeee',
      'valign' => 'center',
      "borderSize" => "3",
      "borderColor" => "#cccccc"
    ],
    'text' => [
      'font' => ['bold' => true, 'color' => '#1c2833', 'size' => 10],
      'position' => ['align' => 'center', 'spaceBefore' => 70, 'spaceAfter' => 70]
    ]
  ];
  const TABLE_CELL_STYLES = [];
}
