<?php
namespace Drupal\cri_php_word\resources\styles\themes;

use Drupal\cri_php_word\resources\styles\DefaultStyleTableInterface;

class DefaultStyleGreyTheme implements DefaultStyleTableInterface {
  const TABLE_TITLE_STYLES = [
    'cell' => [
      'borderColor' => '#ffffff',
      'borderSize' => 1
    ],
    'text' => [
      'font' => [
        'bold' => true,
      ]
    ]
  ];
  const TABLE_STYLES = [
    'table' => [
      'borderSize' => 0,
      'bgColor' => '#eeeeee'
    ],
    'cell' => [
      'bgColor' => '#eeeeee',
      "valign" => "center",
      "borderSize" => "3",
      "borderColor" => "#cccccc"
    ],
    'text' => [
      'font' => [
        'bold' => 'false',
        "color" => "#566573"
      ],
      'position' => [
        'align' => 'center',
        'spaceBefore' => 20,
        'spaceAfter' => 20
      ]
    ]
  ];
  const TABLE_HEAD_STYLES = [
    'cell' => [
      'bgColor' => '#eeeeee',
      'valign' => 'center',
      "borderSize" => "3",
      "borderColor" => "#cccccc"
    ],
    'text' => [
      'font' => ['bold' => true, 'color' => '#1c2833', 'size' => 9],
      'position' => ['align' => 'center', 'spaceBefore' => 20, 'spaceAfter' => 20]
    ]
  ];
  const TABLE_CELL_STYLES = [
    'header' => [
       0 => [
          0 => [
            'repeat' => 'FULL_COLUMN',
            'styles' => [
              'cell' => [
                'bgColor' => '#bdc3c7',
              ]
            ]
          ]
       ]
    ],
    'body' => [
      0 => [
        0 => [
          'repeat' => 'FULL_COLUMN',
          'styles' => [
            'cell' => [
              'bgColor' => '#bdc3c7',
            ],
            'text' => [
              'font' => [
                'color' => '#1c2833',
                'bold' => true,
              ]
            ]
          ]
        ]
      ],
      3 => [
        1 => [
          'repeat' => 'NONE',
          'styles' => [
            'cell' => [
              'bgColor' => '#8e44ad',
            ],
            'text' => [
              'font' => [
                'color' => '#ffffff'
              ]
            ]
          ]
        ],
      ],
      4 => [
        3 => [
          'repeat' => 'NONE',
          'styles' => [
            'cell' => [
              'bgColor' => '#8e44ad',
            ],
            'text' => [
              'font' => [
                'color' => '#ffffff'
              ]
            ]
          ]
        ],
      ],
      5 => [
        5 => [
          'repeat' => 'NONE',
          'styles' => [
            'cell' => [
              'bgColor' => '#8e44ad',
            ],
            'text' => [
              'font' => [
                'color' => '#ffffff'
              ]
            ]
          ]
        ],
      ]
    ],
    'footer' => []
  ];
}
