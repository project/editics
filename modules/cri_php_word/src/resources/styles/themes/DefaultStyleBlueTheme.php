<?php
namespace Drupal\cri_php_word\resources\styles\themes;

use Drupal\cri_php_word\resources\styles\DefaultStyleTableInterface;

class DefaultStyleBlueTheme implements DefaultStyleTableInterface {
  const TABLE_TITLE_STYLES = [
    'cell' => [
      'bgColor' => '#ffffff',
      'borderColor' => '#ffffff',
      'borderSize' => 1
    ],
    'text' => [
      'font' => [
        'bold' => true,
        'size' => 12,
        'underline' => 'single'
      ],
      'position' => [
        'align' => 'left'
      ]
    ]
  ];
  const TABLE_STYLES = [
    'table' => [
      'borderSize' => 0,
      'bgColor' => '#fdfefe'
    ],
    'cell' => [
      'bgColor' => '#fdfefe',
      "valign" => "center",
      "borderSize" => "4",
      "borderColor" => "#5d6d7e"
    ],
    'text' => [
      'font' => [
        'bold' => 'false',
        "color" => "#99a3a4"
      ],
      'position' => [
        'align' => 'center'
      ]
    ]
  ];
  const TABLE_HEAD_STYLES = [
    'cell' => [
      'bgColor' => '#99a3a4',
      'valign' => 'center',
      "borderSize" => "4",
      "borderColor" => "#5d6d7e"
    ],
    'text' => [
      'font' => ['bold' => true, 'color' => '#ffffff'],
      'position' => ['align' => 'center']
    ]
  ];
  const TABLE_CELL_STYLES = [
    'header' => [
       1 => [
          0 => [
            'repeat' => 'NONE',
            'styles' => [
              'cell' => [
                'bgColor' => '#5dade2',
              ]
            ]
          ]
       ]
    ],
    'body' => [
      0 => [
        0 => [
          'repeat' => 'FULL_COLUMN',
          'styles' => [
            'cell' => [
              'bgColor' => '#5dade2',
            ],
            'text' => [
              'font' => [
                'color' => '#ffffff'
              ]
            ]
          ]
        ]
      ],
      3 => [
        1 => [
          'repeat' => 'NONE',
          'styles' => [
            'cell' => [
              'bgColor' => '#8e44ad',
            ],
            'text' => [
              'font' => [
                'color' => '#ffffff'
              ]
            ]
          ]
        ],
      ],
      4 => [
        3 => [
          'repeat' => 'NONE',
          'styles' => [
            'cell' => [
              'bgColor' => '#8e44ad',
            ],
            'text' => [
              'font' => [
                'color' => '#ffffff'
              ]
            ]
          ]
        ],
      ],
      5 => [
        5 => [
          'repeat' => 'NONE',
          'styles' => [
            'cell' => [
              'bgColor' => '#8e44ad',
            ],
            'text' => [
              'font' => [
                'color' => '#ffffff'
              ]
            ]
          ]
        ],
      ]
    ],
    'footer' => [
      0 => [
        'styles' => [
          'cell' => [
            'bgColor' => '#d9ffffff',
            'borderColor' => '#ffffff',
            'borderTopColor' => '#5d6d7e'
          ],
          'text' => [
            'font' => [
              'color' => '#99a3a4',
              'align' => 'right',
              'bold' => true
            ]
          ]
        ]
      ],
      1 => [
        'repeat' => 'FULL_ROW',
        'styles' => [
          'cell' => [
            'bgColor' => '#5dade2',
          ],
          'text' => [
            'font' => [
              'color' => '#ffffff',
              'bold' => true
            ]
          ]
        ]
      ]
    ]
  ];
}
