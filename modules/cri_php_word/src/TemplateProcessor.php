<?php
namespace Drupal\cri_php_word;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use PhpOffice\PhpWord\Exception\CopyFileException;
use PhpOffice\PhpWord\Exception\CreateTemporaryFileException;
use PhpOffice\PhpWord\Shared\XMLWriter;
use PhpOffice\PhpWord\TemplateProcessor as Base;

class TemplateProcessor extends Base
{
  use StringTranslationTrait;
  /**
   * TemplateProcessor constructor.
   * @param $documentTemplate
   * @throws CopyFileException
   * @throws CreateTemporaryFileException
   */
  public function __construct($documentTemplate)
  {
    parent::__construct($documentTemplate);
  }

  public function makeWord($item, array $context) {
    if (!empty($item->type)) {
      $className = 'Drupal\cri_php_word\elements\\' . ucfirst($item->type);
      if (class_exists($className)) {
        $class = new $className();
        if (method_exists($class, 'process')) {
          return $class->process($this, $item, $context);
        }else {
          return ['errorCode' => 101, 'message' => $this->t('process method not found!')];
        }
      }else {
        return ['errorCode' => 202, 'message' => $this->t('@type type not found!', ['@type' => $item->type])];
      }
    }
  }

  /**
   * Return reals variables in the model
   * @return array
   */
  public function getVariableCountParts() {
    $variables = $this->getVariablesForPart($this->tempDocumentMainPart);
    $tmpVariables = [];
    foreach ($this->tempDocumentHeaders as $headerXML) {
      $tmpVariables = array_merge(
        $tmpVariables,
        $this->getVariablesForPart($headerXML)
      );
    }

    foreach ($this->tempDocumentFooters as $footerXML) {
      $tmpVariables = array_merge(
        $tmpVariables,
        $this->getVariablesForPart($footerXML)
      );
    }
    $tmpVariables = array_unique($tmpVariables);
    return array_count_values(array_merge($variables, $tmpVariables));
  }

  /**
   * Add complexes blocks.
   *
   * @param $search
   * @param array $complexTypes
   * @return void
   */
  public function setComplexBlocks($search, array $complexTypes): void
  {
    $complexContent = '';
    foreach ($complexTypes as $complexType) {
      $elementName = substr(get_class($complexType), strrpos(get_class($complexType), '\\') + 1);
      $objectClass = 'PhpOffice\\PhpWord\\Writer\\Word2007\\Element\\' . $elementName;

      $xmlWriter = new XMLWriter();
      /** @var \PhpOffice\PhpWord\Writer\Word2007\Element\AbstractElement $elementWriter */
      $elementWriter = new $objectClass($xmlWriter, $complexType, false);
      $elementWriter->write();

      $complexContent .= $xmlWriter->getData();
    }

    $this->replaceXmlBlock($search, $complexContent, 'w:p');
  }
}
