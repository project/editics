<?php
namespace Drupal\cri_php_word\elements;

use Drupal\cri_php_word\TemplateProcessor;
use PhpOffice\PhpWord\Element\TextRun;

class Checkbox implements EditorProcessInterface
{
  /**
   * @param TemplateProcessor $templateProcess
   * @param $item
   * @return mixed
   */
  public function process(TemplateProcessor &$templateProcess, $item, array $context)
  {
    $format = $context['format'] ?? 'docx';
    $val = !empty($item->value->val) ? $item->value->val : '';
    if (!empty($item->value->checked)) {
      $val = $format == 'docx' ? '☒   ' . $val : '☒ ' . $val;
    }else {
      $val = $format == 'docx' ? '☐   ' . $val : '☐ ' . $val;
    }
    $styles = !empty($item->value->styles) ? json_decode(json_encode($item->value->styles), TRUE) : [];
    $inline = new TextRun();
    /*$inline->addFormField('checkbox')
      ->setDefault(true)
      ->setValue($val)
      ->setFontStyle(['size' => 11]);
    */
    $inline->addText($val, $styles);
    $templateProcess->setComplexValue($item->id, $inline);
    return 0;
  }
}
