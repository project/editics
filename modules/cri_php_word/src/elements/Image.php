<?php
namespace Drupal\cri_php_word\elements;

use PhpOffice\PhpWord\Settings;

use Drupal\cri_php_word\TemplateProcessor;
use Psy\Exception\ErrorException;

class Image implements EditorProcessInterface
{
  /**
   * @param TemplateProcessor $templateProcess
   * @param $item
   * @return mixed|void
   */
  public function process(TemplateProcessor &$templateProcess, $item, array $context)
  {
    if (!empty($item->extension)) {
      $fileName = tempnam(Settings::getTempDir(), 'ImageFileTmp') . '.' .  $item->extension;
      file_put_contents($fileName, base64_decode($item->content));
      if (file_exists($fileName)) {
        $templateProcess->setImageValue($item->id, $fileName);
        unlink($fileName);
      }
      return 0;
    }
    return new ErrorException("Merci de renseigner l'extension de votre image.");
  }
}
