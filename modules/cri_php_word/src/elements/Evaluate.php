<?php
namespace Drupal\cri_php_word\elements;

use Drupal\cri_php_word\TemplateProcessor;
use PhpOffice\PhpWord\Element\TextRun;

class Evaluate implements EditorProcessInterface
{
  /**
   * @param TemplateProcessor $templateProcess
   * @param $item
   * @return mixed|void
   */
  public function process(TemplateProcessor &$templateProcess, $item, array $context): int
  {
    $v = "";
    $val = !empty($item->value->val) ? $item->value->val : (!empty($item->value) ? $item->value : '');
    $styles = !empty($item->value->styles) ? json_decode(json_encode($item->value->styles), TRUE) : [];
    eval('$v = ' . $val . ';');
    if (!empty($styles)) {
      $inline = new TextRun();
      $inline->addText($v, $styles);
      $templateProcess->setComplexValue($item->id, $inline);
    }else {
      $templateProcess->setValue($item->id, $v);
    }
    return 0;
  }
}
