<?php
namespace Drupal\cri_php_word\elements;

use Drupal\cri_php_word\TemplateProcessor;
use PhpOffice\PhpWord\Element\Section;

class Listpuce implements EditorProcessInterface
{
  /**
   * @param TemplateProcessor $templateProcess
   * @param $item
   * @return mixed
   */
  public function process(TemplateProcessor &$templateProcess, $item, array $context)
  {
    if (!empty($item->value) && is_array($item->value)) {
      $sectiobn = new Section(1);
      $inline = $sectiobn->addListItemRun(0, 'PHPWordListType5');
      $format = $context['format'] ?? 'docx';
      foreach ($item->value as $v) {
        $val = !empty($v->val) ? '➤   ' . $v->val : '';
        $styles = !empty($v->styles) ? json_decode(json_encode($v->styles), TRUE) : [];
        $inline->addText($val, $styles);
        $inline->addTextBreak(1);
      }
      $templateProcess->setComplexBlock($item->id, $inline);
    }
    return 0;
  }
}
