<?php
namespace Drupal\cri_php_word\elements;

use Drupal\cri_php_word\resources\ResourcesManager;
use Drupal\cri_php_word\resources\styles\DefaultStyleTableInterface;
use Drupal\cri_php_word\TemplateProcessor;
use PhpOffice\PhpWord\SimpleType\TblWidth;
use PhpOffice\PhpWord\Element\Table as oTable;
use PhpOffice\PhpWord\Style\Cell;
use PhpOffice\PhpWord\Style\TablePosition;

class Table implements EditorProcessInterface
{
  /**
   * @param TemplateProcessor $templateProcess
   * @param $item
   * @return mixed|void
   */
  public function process(TemplateProcessor &$templateProcess, $elt, array $context)
  {
    $defaultStyleTable = ResourcesManager::getResource('DEFAULT_THEME');
    $stylesTab = $defaultStyleTable::TABLE_STYLES['table'];

    $items = !empty($elt->multiple) ? $elt->tables : [$elt];

    $tabs = [];
    foreach ($items as $key => $item) {
      if (!empty($item->styles->table->table)) {
        $stylesTab = json_decode(json_encode($item->styles->table->table), true);
      }
      $stylesTab = array_merge( $stylesTab, [
        'unit' => !empty($stylesTab['unit']) ? $stylesTab['unit'] : TblWidth::TWIP,
        'width' => !empty($stylesTab['width']) ? $stylesTab['width'] : 10450,
        'position' => !empty($stylesTab['position']) ? $stylesTab['position'] : [
          'vertAnchor' => TablePosition::HANCHOR_TEXT,
          'tblpXSpec' => TablePosition::XALIGN_CENTER,
        ],
      ]);

      $tab = new oTable($stylesTab);

      if (!empty($item->theme)) {
        $tmpsDefaultStyleTable = ResourcesManager::getResource($item->theme);
        if ($tmpsDefaultStyleTable instanceof DefaultStyleTableInterface) {
          $defaultStyleTable = $tmpsDefaultStyleTable;
        }
      }

      $hStyles = $defaultStyleTable::TABLE_HEAD_STYLES;
      // Header
      if (is_array($item->value->header)) {
        if (!empty($item->styles->header)) {
          $hStyles = json_decode(json_encode($item->styles->header), true);
        }
        $hLonger = max($item->value->header);
        $nbr = count($hLonger->tr);
        $tStyles = $defaultStyleTable::TABLE_TITLE_STYLES;
        if ($key > 0) {
          $tab->addRow();
          $tab->addCell(NULL, array_merge(['gridSpan' => $nbr], $tStyles['cell']))
            ->addText('');
        }

        if (!empty($item->title)) {
          $tab->addRow();
          if (!empty($item->title->styles)) {
            $tCustomStyles = json_decode(json_encode($item->title->styles), true);
            $this->customStyles($tCustomStyles, $tStyles);
          }
          $tab->addCell(NULL, array_merge(['gridSpan' => $nbr], $tStyles['cell']))
            ->addText($item->title->value, $tStyles['text']['font'], $tStyles['text']['position']);
        }
        $cellThemeStyles = $defaultStyleTable::TABLE_CELL_STYLES;
        foreach ($item->value->header as $row => $r) {
          $tab->addRow();
          foreach ($r->tr as $col => $c) {
            $tmpHStyles = $hStyles;
            if (!empty($cellThemeStyles['header'][$row][$col])) {
              $theme = $cellThemeStyles['header'][$row][$col];
              $this->themeStyles($theme, $tmpHStyles);
              if (!empty($theme['repeat'])) {
                switch ($theme['repeat']) {
                  case 'FULL_COLUMN':
                    $cellThemeStyles['header'][($row + 1)][$col] = $theme;
                    break;
                  case 'FULL_ROW':
                    $cellThemeStyles['header'][$row][($col+1)] = $theme;
                    break;
                  default:
                    break;
                }
              }
            }
            if (!empty($c->styles)) {
              $cellCustomStyles = json_decode(json_encode($c->styles), true);
              $this->customStyles($cellCustomStyles, $tmpHStyles);
            }
            $this->createCell($tab, $c, $tmpHStyles['cell'], $tmpHStyles['text'], $context);
          }
        }
      }
      // Body
      if (!empty($item->value->body)) {
        $bStyles = $defaultStyleTable::TABLE_STYLES;
        if (!empty($item->styles->table)) {
          $bStyles = json_decode(json_encode($item->styles->table), true);
        }
        foreach ($item->value->body as $row => $r) {
          $tab->addRow();
          foreach ($r->tr as $col => $ct) {
            $tmpBStyles = $bStyles;
            $theme = NULL;
            if (!empty($cellThemeStyles['body'][$row][$col])) {
              $theme = $cellThemeStyles['body'][$row][$col];
            }
            if ($row === (count($item->value->body) - 1)) {
              if (!empty($cellThemeStyles['footer'][$col])) {
                $theme = $cellThemeStyles['footer'][$col];
              }
            }
            if (!empty($theme)) {
              $this->themeStyles($theme, $tmpBStyles);
              if (!empty($theme['repeat'])) {
                switch ($theme['repeat']) {
                  case 'FULL_COLUMN':
                    $cellThemeStyles['body'][($row + 1)][$col] = $theme;
                    break;
                  case 'FULL_ROW':
                    $cellThemeStyles['body'][$row][($col + 1)] = $theme;
                    break;
                  default:
                    break;
                }
              }
            }
            if (!empty($ct->styles)) {
              $rtextCustomStyles = json_decode(json_encode($ct->styles), true);
              $this->customStyles($rtextCustomStyles, $tmpBStyles);
            }
            $this->createCell($tab, $ct, $tmpBStyles['cell'], $tmpBStyles['text'], $context);
          }
        }
      }

      $tabs[] = $tab;
    }
    $templateProcess->setComplexBlocks($elt->id, $tabs);
    return 0;
  }

  /**
   * @param $table
   * @param $cell
   * @param $cellStyles
   * @param $textStyles
   */
  private function createCell(&$table, $cell, $cellStyles, $textStyles, array $context) {
    $width = !empty($cell->width) ? $cell->width : 300;
    $font = !empty($textStyles['font']) ? $textStyles['font'] : [];
    $position = !empty($textStyles['position']) ? $textStyles['position'] : [];
    if (!empty($cell->colspan)) {
      $cellStyles = array_merge(['gridSpan' => $cell->colspan], $cellStyles);
    }
    $c = $table->addCell($width, $cellStyles);
    if (empty($position['spaceAfter']))
      $position['spaceAfter'] = 70;
    if (empty($position['spaceBefore']))
      $position['spaceBefore'] = 70;
    if (!is_array($cell->value)) {
      $v = $cell->value;
      if(!empty($cell->value->val)) {
        $v = $v->val;
      }
      $c->addText(
        $v,
        $font,
        $position);
      if (!empty($cell->value->legend)) {
        $tmpBStyles = [
          'text' => [
            'position' => ['align' => 'center', 'spaceBefore' => 10, 'spaceAfter' => 40]
          ]
        ];
        if ($cell->value->legend->styles) {
          $lgStyles = json_decode(json_encode($cell->value->legend->styles), true);
          $this->customStyles($lgStyles, $tmpBStyles);
        }

        $c->addText(
          $cell->value->legend->val,
          $tmpBStyles['text']['font'],
          $tmpBStyles['text']['position']);
      }
    }else {
      $format = $context['docx'] ?? 'docx';
      foreach ($cell->value as $l) {
        $val = $l->val;
        if (!empty($l->type)) {
          switch ($l->type) {
            case 'checkbox':
              $val = $format == 'docx' ? (!empty($l->checked) ? '☒    ' . $l->val : '☐    ' . $l->val) : (!empty($l->checked) ? '☒ ' . $l->val : '☐ ' . $l->val);
              break;
            case 'label':
              $val = !empty($l->label) ? $l->label . ' : ' . $l->val : $l->val;
              break;
            case 'puce':
              $val = $format == 'docx' ? (!empty($l->puce) ? $l->puce . '   ' . $l->val : '➤    ' . $l->val) : (!empty($l->puce) ? $l->puce . ' ' . $l->val : '➤ ' . $l->val);
              break;
            default:
              // TODO
              break;
          }
        }

        $c->addText($val, $font, $position);
      }
    }
    if (!empty($cellStyles['vMerge'])) {
      if (!empty($cell->dir)) {
        $c->getStyle()->setTextDirection(Cell::TEXT_DIR_BTLR);
      }
    }
  }

  /**
   * @param $customStyles
   * @param $tmpStyles
   */
  private function customStyles($customStyles, &$tmpStyles) {
    foreach ($customStyles as $k => $val) {
      if (is_array($val)) {
        switch ($k) {
          case 'text':
            foreach ($val as $d => $vd) {
              if (is_array($vd)) {
                foreach ($vd as $t => $vt) {
                  $tmpStyles[$k][$d][$t] = $vt;
                }
              }
            }
            break;
          default:
            foreach ($val as $d => $vd) {
              $tmpStyles[$k][$d] = $vd;
            }
            break;
        }
      }
    }
  }

  /**
   * @param $customStyles
   * @param $tmpStyles
   */
  private function themeStyles($theme, &$tmpStyles) {
    foreach ($theme['styles'] as $k => $val) {
      if (is_array($val)) {
        switch ($k) {
          case 'text':
            foreach ($val as $d => $vd) {
              if (is_array($vd)) {
                foreach ($vd as $t => $vt) {
                  $tmpStyles[$k][$d][$t] = $vt;
                }
              }
            }
            break;
          default:
            foreach ($val as $d => $vd) {
              $tmpStyles[$k][$d] = $vd;
            }
            break;
        }
      }
    }
  }
}
