<?php
namespace Drupal\cri_php_word\elements;

use Drupal\cri_php_word\TemplateProcessor;
use PhpOffice\PhpWord\Element\TextRun;

class Percent extends Text
{
  /**
   * @param TemplateProcessor $templateProcess
   * @param $item
   * @return mixed|void
   */
  public function process(TemplateProcessor &$templateProcess, $item, array $context): int
  {
    return parent::process($templateProcess, $item, $context);
  }
}
