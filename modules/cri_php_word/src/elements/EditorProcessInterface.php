<?php
namespace Drupal\cri_php_word\elements;

use Drupal\cri_php_word\TemplateProcessor;

/**
 * Interface EditorProcessInterface
 * @package Drupal\cri_php_word\convertor
 */
interface EditorProcessInterface
{
  /**
   * @param TemplateProcessor $templateProcess
   * @param $item
   * @return mixed
   */
  public function process(TemplateProcessor &$templateProcess, $item, array $context);
}
