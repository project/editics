<?php
namespace Drupal\cri_php_word\convertor;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use PhpOffice\PhpWord\IOFactory;
use PhpOffice\PhpWord\Settings;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class Dompdf extends Convertor
{
  use StringTranslationTrait;

  /**
   * @param $destination
   * @param null $format
   * @return mixed|void
   */
  public function convert($destination, $format = NULL): array
  {
    if (empty($this->files)) {
      return ['errorCode' => 1, 'message' => $this->t('No file to convert.')];
    }

    try {
      Settings::setPdfRendererName(Settings::PDF_RENDERER_DOMPDF);
      Settings::setPdfRendererPath('.');

      $pathInfos = pathinfo($this->files[0]);
      $outputFile = $pathInfos['dirname'] . '/' . $pathInfos['filename'] . '.pdf';

      $phpWord = IOFactory::load($this->files[0]);
      $phpWord->save($outputFile, 'PDF');
      return ['errorCode' => 0, 'files' => $this->destinations];
    } catch (ProcessFailedException $ex) {
      return ['errorCode' => 1, 'message' => $ex->getMessage()];
    }
  }
}
