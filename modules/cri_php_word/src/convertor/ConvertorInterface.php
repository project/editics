<?php


namespace Drupal\cri_php_word\convertor;


interface ConvertorInterface
{
  /**
   * @param $sourceFileName
   * @param $destination
   * @param null $format
   * @return mixed
   */
  public function convert($destination, $format = NULL): array;

  /**
   * @param $fileName
   * @return void
   */
  public function addFile($fileName);

  /**
   * @return void
   */
  public function deleteSourceFiles();
}
