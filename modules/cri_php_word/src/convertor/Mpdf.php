<?php
namespace Drupal\cri_php_word\convertor;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use PhpOffice\PhpWord\IOFactory;
use PhpOffice\PhpWord\Settings;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class Mpdf extends Convertor
{
  use StringTranslationTrait;

  /**
   * @param $destination
   * @param null $format
   * @return mixed|void
   */
  public function convert($destination, $format = NULL): array
  {
    if (empty($this->files)) {
      return ['errorCode' => 1, 'message' => $this->t('No file to convert.')];
    }

    try {
      Settings::setPdfRendererName(Settings::PDF_RENDERER_MPDF);
      Settings::setPdfRendererPath('vendor/mpdf/mpdf');

      $pathInfos = pathinfo($this->files[0]);
      $outputFile = $pathInfos['dirname'] . '/' . $pathInfos['filename'] . '.pdf';

      $content = IOFactory::load($this->files[0]);
      $pdfWriter = IOFactory::createWriter($content, 'PDF');
      $pdfWriter->save($outputFile);

      return ['errorCode' => 0, 'files' => $this->destinations];
    } catch (ProcessFailedException $ex) {
      return ['errorCode' => 1, 'message' => $ex->getMessage()];
    }
  }
}
