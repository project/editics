<?php
namespace Drupal\cri_php_word\convertor;

class Convertor implements ConvertorInterface
{
  protected array $files = [];
  protected array $destinations = [];
  /**
   * @param $destination
   * @param null $format
   * @return mixed|void
   */
  public function convert($destination, $format = NULL): array { }

  /**
   * @param $fileName
   * @return void
   */
  public function addFile($fileName)
  {
    $this->files[] = $fileName;
    $this->destinations[] = str_ireplace('.docx', '.pdf', $fileName);
  }

  /**
   *
   */
  public function deleteSourceFiles()
  {
    foreach ($this->files as $f) {
      if (file_exists($f)) {
        unlink($f);
      }
    }
  }
}
