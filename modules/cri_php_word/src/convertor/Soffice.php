<?php
namespace Drupal\cri_php_word\convertor;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class Soffice extends Convertor
{
  use StringTranslationTrait;

  /**
   * @param $destination
   * @param null $format
   * @return mixed|void
   */
  public function convert($destination, $format = NULL): array
  {
    if (empty($this->files)) {
      return ['errorCode' => 1, 'message' => $this->t('No file to convert.')];
    }
    $PID = uniqid();
    $port = rand(10000, 20000);
    $outdir = Drupal::service('file_system')->dirname($this->files[0]);

    // Commande avec multiprocess
    /*
    $cmd = 'export HOME=/tmp && soffice ' .
      '--headless ' .
      '--nologo ' .
      '"-env:UserInstallation=file:///tmp/lib0_Conversion__' . $PID . '" ' .
      '--accept="socket,port=' . $port. ';urp" ' .
      '--convert-to pdf:writer_pdf_Export --outdir ' . $outdir . ' ' . implode(' ', $this->files);
*/
    // Commande sans multiprocess

    $process = new Process(array_merge([
      'soffice',
      '--convert-to',
      'pdf:writer_pdf_Export',
      '--headless',
      '--nologo',
      '"-env:UserInstallation=file:///tmp/lib0_Conversion__' . $PID . '"',
      '--accept="socket,port=' . $port. ';urp"',
      '--outdir',
      $outdir,
    ], $this->files), '/tmp');
    try {
      $process->mustRun();
      return ['errorCode' => 0, 'files' => $this->destinations];
    } catch (ProcessFailedException $ex) {
      return ['errorCode' => 1, 'message' => $ex->getMessage()];
    }
  }
}
