<?php

namespace Drupal\cri_php_word\convertor;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Ilovepdf\Ilovepdf;

class Api extends Convertor
{
  use StringTranslationTrait;

  /**
   * @param $destination
   * @param null $format
   * @return array
   */
  public function convert($destination, $format = NULL): array
  {
    // $ilovePdf = new Ilovepdf('project_public_0b9b7ed4db85199c3fcf09752f93309e_rxTLr485e31e83ee39cc0027ff4f4eea125bc', 'secret_key_dc009c4e88531192ce5ede01494efef7_oAKjKf149bf46bd64b9e6ef795ec65ee6be2f');
    // $myTask = $ilovePdf->newTask('officepdf');
    return ['errorCode' => 1, 'message' => $this->t('Functionality under development!')];
  }
}
