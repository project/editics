<?php

namespace Drupal\cri_php_word\convertor;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class Adobe extends Convertor
{
  use StringTranslationTrait;

  /**
   * @param $destination
   * @param null $format
   * @return array
   */
  public function convert($destination, $format = NULL): array
  {
    if (empty($this->files)) {
      return ['errorCode' => 1, 'message' => $this->t('No files to convert.')];
    }
    if (count($this->files) > 1) {
      return ['errorCode' => 1, 'message' => $this->t('This converter can only convert one file at a time.')];
    }
    $cmd = 'node ' .
      'nodejs/pdfservices-node-sdk-samples/src/createpdf/create-pdf-from-docx.js ' .
      $this->files[0] . ' ' .
      $destination;
    $process = new Process($cmd);
    try {
      $process->mustRun();
      return ['errorCode' => 0, 'files' => [$destination]];
    } catch (ProcessFailedException $ex) {
      return ['errorCode' => 1, 'message' => $ex->getMessage()];
    }
  }
}
