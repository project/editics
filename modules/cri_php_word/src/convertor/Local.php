<?php

namespace Drupal\cri_php_word\convertor;

use Drupal\Core\StringTranslation\StringTranslationTrait;

class Local extends Convertor
{
  use StringTranslationTrait;

  /**
   * @param $destination
   * @param null $format
   * @return array
   */
  public function convert($destination, $format = NULL): array
  {
    // TODO: Implement convert() method.
    /*
    $options = [
      'headers' => [
        'Accept' => 'application/json',
        'Content-Type' => 'application/json',
        'params' => json_encode([
          'input' => $sourceFileName,
          'output' => $destination,
        ])
      ],
    ];
    $response = Drupal::httpClient()->post('http://localhost:7172/', $options)->getBody();
    $files[$model->docKey] = base64_encode($response->getContents());;
    */
    return ['errorCode' => 1, 'message' => $this->t('Functionality under development!')];
  }
}
