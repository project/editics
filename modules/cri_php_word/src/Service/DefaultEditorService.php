<?php

namespace Drupal\cri_php_word\Service;

use Drupal;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\editics\Service\interfaces\DefaultLoggerServiceInterface;
use Drupal\cri_php_word\TemplateProcessor;
use Drupal\Core\File\FileSystem;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\cri_php_word\Service\error\DefaultRestErrorServiceInterface;
use Drupal\user\Entity\User;
use PhpOffice\PhpWord\Exception\CopyFileException;
use PhpOffice\PhpWord\Exception\CreateTemporaryFileException;
use PhpOffice\PhpWord\Exception\Exception;
use Drupal\file\Entity\File;
use Unoconv\Unoconv;

/**
 * Class DefaultEditorService.
 */
class DefaultEditorService {

  use StringTranslationTrait;
  /**
   * Drupal\editics\Service\error\DefaultRestErrorServiceInterface definition.
   *
   * @var DefaultRestErrorServiceInterface
   */
  protected DefaultRestErrorServiceInterface $defaultRestErrorService;
  /**
   * @var DefaultLoggerServiceInterface
   */
  protected DefaultLoggerServiceInterface $defaultLoggerService;
  /**
   * @var $templateUri
   */
  private $templateUri;
  /**
   * @var $templateProcessor
   */
  private $templateProcessor;
  /**
   * @var $storageActive
   */
  private $storageActive = FALSE;
  /**
   * @var bool
   */
  private $anonymous = FALSE;
 /**
  *  Request data
  */
  private $request;

  /**
   * Constructs a new DefaultEditorService object.
   * @param DefaultRestErrorServiceInterface $default_rest_error_service
   * @param DefaultLoggerServiceInterface $default_logger_service
   */
  public function __construct(DefaultRestErrorServiceInterface $default_rest_error_service, DefaultLoggerServiceInterface $default_logger_service) {
    $this->defaultRestErrorService = $default_rest_error_service;
    $this->defaultLoggerService = $default_logger_service;
    $user = User::load(Drupal::currentUser()->id());
    if(in_array('anonymous', $user->getRoles())) {
      $this->anonymous = TRUE;
      $user = user_load_by_name('admin@api.org');
      if (!empty($user)) {
        user_login_finalize($user);
      }
    }
    if ($user && $user->hasField('field_user_storage_docs') && !empty($user->get('field_user_storage_docs')->getString())) {
      $this->storageActive = TRUE;
    }
  }

  /**
   * @param $contentTemplate
   * @return bool
   */
  private function initialize($contentTemplate) {
    $folder = Drupal::service('file_system')->realpath('private://') . '/modeles';
    $e = Drupal::service('file_system')->prepareDirectory($folder, FileSystem::CREATE_DIRECTORY);
    if ($e) {
      $this->templateUri = $folder . '/' . uniqid() . '_model.docx';
      $content = base64_decode($contentTemplate);
      if (preg_match('/word\/document/i', $content)) {
        file_put_contents($this->templateUri, $content);
        return TRUE;
      } else {
        $this->defaultRestErrorService->add('cri_file_save_error', $this->t('Votre modèle n\'est pas de type [.docx]'));
      }
    }

    $this->defaultRestErrorService->add('cri_file_init_error', $this->t('Il se peut que le répertoire private soit abscent de votre projet ou les permissions ne permettent pas la création. Merci de le créer avant de continuer.'));

    return FALSE;
  }

  /**
   * @param $folderName
   * @param $sourceFile
   * @return Drupal\Core\Entity\EntityBase|Drupal\Core\Entity\EntityInterface
   */
  private function finalize($folderName, $sourceFile, $isDownload) {
    $folder = $isDownload ? 'public://': 'private://' . $folderName;
    Drupal::service('file_system')->prepareDirectory($folder, FileSystem::CREATE_DIRECTORY);
    $fileUri = $folder . '/' . basename($sourceFile);
    $fileSource = File::create([
      'filename' => basename($sourceFile),
      'uri' => $sourceFile,
      'status' => 'FILE_STATUS_PERMANENT',
      'langcode' => 'fr'
    ]);
    try {
      $fileSource->save();
      $file = Drupal::service('file.repository')->move($fileSource, $fileUri);
      if (!empty($file)) {
        $file->setFilename(basename($fileUri));
        $file->setPermanent();
        return $file;
      }
    } catch (EntityStorageException $e) {
      $this->defaultRestErrorService->add('cri_file_save_error', $e->getMessage());
      return $e->getMessage();
    }
    return NULL;
  }

  /**
   * @param $request
   * @return false|string
   */
  public function edit($request) {
    $files = [];
    $toConvert = [];
    $request->download = $request->download ?? FALSE;
    $request->remote = $request->remote ?? FALSE;
    $this->request = $request;

    // Si ces flags sont passés, on force le stockage selon ces flags.
    if (isset($request->remote) && isset($request->download)) {
      $this->storageActive = !$request->remote && $request->download;
    }

    foreach ($request->models as $mNum => &$model) {
      $model->codeError = 0; // On debut sans erreur
      $model->msgError = '';
      // Création du template avec le contenu $contentTemplate
      $isInit = $this->initialize($model->contentFile);
      // Si l'initialisation echoue
      if (!empty($this->defaultRestErrorService->get())) {
        $errors = $this->defaultRestErrorService->get();
        $error = reset($errors);
        $model->msgError = $error->__toString();
        $model->codeError = 1;
        $files[$model->docKey] = ['errorCode' => $model->codeError, 'message' => $model->msgError];
      }

      if ($isInit && file_exists($this->templateUri)) {
        try {
          $this->templateProcessor = new TemplateProcessor($this->templateUri);
          $this->templateProcessor->setUpdateFields(true);
          $variables = $this->templateProcessor->getVariableCountParts();
          unlink($this->templateUri);
          if (!empty($model->settings)) {
            foreach ($model->settings->data as $item) {
              if (empty($item->id) || !array_key_exists($item->id, $variables)) {
                continue;
              }
              for ($i = 0; $i < $variables[$item->id]; $i++) {
                $maker = $this->templateProcessor->makeWord($item, ['format' => $model->format]);
                if (!empty($maker)) {
                  $model->codeError = 1;
                  if (method_exists($maker, 'getMessage')) {
                    $model->msgError = $maker->getMessage();
                    $files[$model->docKey] = ['errorCode' => $model->codeError, 'message' => $model->msgError];
                  } else {
                    $model->msgError = $maker;
                    $files[$model->docKey] = ['errorCode' => $model->codeError, 'message' => $model->msgError];
                  }
                }
              }
            }
          }
          if (empty($model->codeError)) {
            try {
              $this->templateProcessor->saveAs($this->templateUri);
              $model->pages = $this->defaultLoggerService->pagesCountDocx($this->templateUri);
              $finalFile = str_ireplace('.docx', '.pdf', $this->templateUri);
              switch ($model->format) {
                case 'pdf':
                  $className = 'Drupal\cri_php_word\convertor\\' . ucfirst($model->convertor);
                  if (class_exists($className)) {
                    $convertor = new $className();
                    if (method_exists($convertor, 'addFile')) {
                      $convertor->addFile($this->templateUri);
                      $toConvert[] = ['conv' => $convertor, 'dest' => $finalFile, 'format' => 'pdf', 'model' => $model->docKey, 'mNum' => $mNum];
                    }else {
                      $model->msgError = $this->t('The requested converter does not exist.');
                      $model->codeError = 1;
                      $files[$model->docKey] = ['errorCode' => $model->codeError, 'message' => $model->msgError];
                    }
                  }
                  break;
                case 'docx':
                  if (file_exists($this->templateUri)) {
                    $file = $this->finalize('documents', $this->templateUri, $request->download);
                    if ($file instanceof File) {
                      unlink($this->templateUri);
                      $model->file = $file->id();
                      if (!empty($request->download) && $this->storageActive) {
                        $files[$model->docKey] = ['errorCode' => 0, 'message' => $this->t('success'), 'file' => Drupal::service('file_url_generator')->generateAbsoluteString($file->getFileUri())];
                      } else {
                        $content = base64_encode(file_get_contents($file->getFileUri()));
                        $files[$model->docKey] = ['errorCode' => 0, 'message' => $this->t('success'), 'file' => ['extension' => $model->format, 'content' => $content]];
                      }
                      // Si le compte ne souhaite pas sauvegarder les document sur nos serveurs
                      if (!$this->storageActive) { // A reactiver lorsqu'une solution de comptage des docx sera trouvé
                        // $file->delete();
                        // $model->file = NULL;
                      }
                    }else {
                      $model->msgError = $file;
                      $model->codeError = 1;
                    }
                  }else {
                    $model->msgError = $this->t('No file generated.');
                    $model->codeError = 1;
                    $files[$model->docKey] = ['errorCode' => $model->codeError, 'message' => $model->msgError];
                  }
                  break;
                default:
                  $model->msgError = $this->t('Type of document not found!');
                  $model->codeError = 1;
                  $files[$model->docKey] = ['errorCode' => $model->codeError, 'message' => $model->msgError];
                  break;
              }
            } catch (Exception $e) {
              $model->msgError = $e->getMessage();
              $model->codeError = 1;
              $files[$model->docKey] = ['errorCode' => $model->codeError, 'message' => $model->msgError];
            }
          }
        } catch (CopyFileException $e) {
          $model->msgError = $e->getMessage();
          $model->codeError = 1;
          $files[$model->docKey] = ['errorCode' => $model->codeError, 'message' => $model->msgError];
        } catch (CreateTemporaryFileException $e) {
          $model->msgError = $e->getMessage();
          $model->codeError = 1;
          $files[$model->docKey] = ['errorCode' => $model->codeError, 'message' => $model->msgError];
        }
      }
    }
    // Traitement des documents à convertir
    if (!empty($toConvert)) {
      foreach ($toConvert as $data) {
        if (method_exists($data['conv'], 'convert')) {
          $result = $data['conv']->convert($data['dest'], $data['format']);
          $data['conv']->deleteSourceFiles();
          if (empty($result['errorCode']) && !empty($result['files'])) {
            foreach ($result['files'] as $fileName) {
              if (file_exists($fileName)) {
                $file = $this->finalize('documents', $fileName, $request->download);
                if ($file instanceof File) {
                  if (file_exists($fileName)) {
                    unlink($fileName);
                  }
                  $request->models[$data['mNum']]->file = $file->id();
                  if (!empty($request->download) && $this->storageActive) {
                    $files[$data['model']] = ['errorCode' => 0, 'message' => $this->t('success'), 'file' => Drupal::service('file_url_generator')->generateAbsoluteString($file->getFileUri())];
                  }else {
                    $content = base64_encode(file_get_contents($file->getFileUri()));
                    $files[$data['model']] = ['errorCode' => 0, 'message' => $this->t('success'), 'file' => ['extension' => $data['format'], 'content' => $content]];
                  }
                  // Si le compte ne souhaite pas sauvegarder les document sur nos serveurs
                  if (!$this->storageActive) { // A reactiver lorsqu'une solution de comptage des docx sera trouvé
                    // $file->delete();
                    // $request->models[$data['mNum']]->file = NULL;
                  }
                } else {
                  $request->models[$data['mNum']]->msgError = $file;
                  $request->models[$data['mNum']]->codeError = 1;
                  $files[$request->models[$data['mNum']]->docKey] = [
                    'errorCode' => $request->models[$data['mNum']]->codeError,
                    'message' => $request->models[$data['mNum']]->msgError];
                }
              }else {
                $request->models[$data['mNum']]->msgError = 'Pas de fichier généré.';
                $request->models[$data['mNum']]->codeError = 1;
                $files[$request->models[$data['mNum']]->docKey] = [
                  'errorCode' => $request->models[$data['mNum']]->codeError,
                  'message' => $request->models[$data['mNum']]->msgError];
              }
            }
          }else {
            $request->models[$data['mNum']]->msgError = $result['message'];
            $request->models[$data['mNum']]->codeError = $result['errorCode'];
            $files[$request->models[$data['mNum']]->docKey] = [
              'errorCode' => $request->models[$data['mNum']]->codeError,
              'message' => $request->models[$data['mNum']]->msgError];
          }
        }
      }
    }
    // Traçage de l'appel API
    if (!empty(Drupal::config('cri_core.settings')->get('logs_actives'))) {
      $this->defaultLoggerService->logger(Drupal::currentUser(), $request);
    }

    if ($this->anonymous) { // Deconnexion
      user_user_logout(Drupal::currentUser());

      // Force la suppression de la session
      Drupal::service('session')->remove('uid');
    }

    // Retour
    if (!empty($files)) {
      return $files;
    }
    return $this->defaultRestErrorService->get();
  }
}
