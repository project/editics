<?php

namespace Drupal\cri_php_word\Service\error;

/**
 * Class DefaultRestErrorService.
 */
class DefaultRestErrorService implements DefaultRestErrorServiceInterface {
  private $_errors;
  /**
   * Constructs a new DefaultRestErrorService object.
   */
  public function __construct() {
    $this->_errors = [];
  }

  public function add($key, $errorString)
  {
    $this->_errors[$key] = $errorString;
  }

  public function set(array $errorArray)
  {
    $this->_errors = $errorArray;
  }

  public function get($key = NULL)
  {
    if (!empty($key)) {
      return $this->_errors[$key];
    }
    return $this->_errors;
  }
}
