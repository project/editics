<?php

namespace Drupal\cri_php_word\Service\error;

/**
 * Interface DefaultRestErrorServiceInterface.
 */
interface DefaultRestErrorServiceInterface {
  /**
   * @param $key
   * @param $errorString
   * @return void
   */
  public function add($key, $errorString);

  /**
   * @param array $errorArray
   * @return void
   */
  public function set(Array $errorArray);

  /**
   * @param $key
   * @return mixed
   */
  public function get($key = NULL);
}
