<?php

namespace Drupal\cri_php_word\Service;

use Drupal;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountProxy;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\editics\Service\interfaces\DefaultLoggerServiceInterface;
use Drupal\cri_php_word\convertor\Soffice;
use Drupal\file\Entity\File;
use Drupal\node\Entity\Node;
use Drupal\user\Entity\User;
use PhpOffice\PhpWord\Shared\ZipArchive;
use SimpleXMLElement;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;

/**
 * Class DefaultRestEditicService.
 */
class DefaultLoggerService implements DefaultLoggerServiceInterface {

  use StringTranslationTrait;
  const _MONTHS = [
    1 => 'Janvier',
    2 => 'Février',
    3 => 'Mars',
    4 => 'Avril',
    5 => 'Mai',
    6 => 'Juin',
    7 => 'Juillet',
    8 => 'Août',
    9 => 'Septembre',
    10 => 'Octobre',
    11 => 'Novembre',
    12 => 'Décembre'
  ];
  /**
   * @param AccountProxyInterface $user
   * @param $request
   */
  function logger(AccountProxyInterface $user, &$request)
  {
    $localSave = Drupal::config('cri_core.settings')->get('log_home_calls');
    if (!$request->remote && empty($localSave)) {
      return [];
    }
    [$globalStatus, $models] = $this->createModels($request);
    $request->list = $models;
    $log = Node::create([
      'title' => $this->t('Appel API'),
      'type' => 'ws_logger',
      'field_ws_log_action' => $request->action,
      'field_ws_log_logger' => $user->id(),
      'field_ws_log_nb_modeles' => count($request->models),
      'field_ws_log_modeles' => $models,
      'field_ws_log_statut' => $globalStatus,
      'field_ws_log_flux' => $globalStatus !== 'success' ? json_encode($request) : '',
      'field_ws_log_service' => $this->getServiceByAction($request->action),
    ]);
    try {
      $log->save();
    } catch (\Exception $exc) {
      error_log($exc->getMessage());
    }
  }

  /**
   * Return formayed logs by user by periode
   *
   * @param $account
   * @param Node $subscribe
   * @param array|null $periode
   * @return mixed
   * @throws \Exception
   */
  public function getLogsFormByUserByPeriode($account, Node $subscribe, $periode = NULL) {
    if (empty($periode)) {
      $periode = $this->getPeriode($subscribe);
    }
    $nodes = $this->selectLogByUserByPeriode($account, $subscribe, $periode);

    $stats = [
      'periode' => ['title' => $this->t('Période'), 'value' => $periode['start']->format('d/m/Y') . ' au ' . $periode['end']->format('d/m/Y')],
      'calls' => ['title' => $this->t('Nombre d\'appels'), 'value' => 0],
      'models' => ['title' => $this->t('Nombre de modèles'), 'value' => 0],
      'docs' => ['title' => $this->t('Nombre des documents'), 'value' => 0],
      'success' => ['title' => $this->t('Nombre d\'appels avec succès'), 'value' => 0],
      'warnings' => ['title' => $this->t('Nombre d\'appels en alerte'), 'value' => 0],
      'errors' => ['title' => $this->t('Nombre d\'appels en erreur'), 'value' => 0],
    ];

    if (!empty($nodes)) {
      $stats['calls']['value'] = count($nodes);
      $nbrMAxPages = intval(Drupal::config('cri_core.settings')->get('nbr_max_pages')) ?? 3;
      foreach ($nodes as $node) {
        $models = $node->get('field_ws_log_modeles')->referencedEntities();
        $stats['models']['value'] += count($models);
        foreach ($models as $model) {
          $nb = intval($model->get('field_ws_modele_nb_pages')->getString());

          if ($nb > $nbrMAxPages) {
            $stats['docs']['value'] +=  ($nb / $nbrMAxPages);
          }else {
            $stats['docs']['value'] += $nb;
          }
        }
        switch ($node->get('field_ws_log_statut')->getString()) {
          case 'success':
            $stats['success']['value']++;
            break;
          case 'warning':
            $stats['warnings']['value']++;
            break;
          case 'error':
            $stats['errors']['value']++;
            break;
        }
      }
    }

    return json_decode(json_encode($stats));
  }

  /**
   * Select logs by user by periode
   *
   * @param $account
   * @param Node $subscribe
   * @param array|null $periode
   * @return array|int
   * @throws \Exception
   */
  public function selectLogByUserByPeriode($account, Node $subscribe, $periode = NULL) {
    if (empty($periode)) {
      $periode = $this->getPeriode($subscribe);
    }

    return $this->getLogs([
      'user_id' => $account->id(),
      'periode' => $periode
    ]);
  }
  /**
   * Calcul tarif par user et par période
   *
   * @param $account
   * @param array $periode
   *
   */
  public function tarifByUserByPeriode($account, $periode) {
    $configTarif = Drupal::config('cri_core.settings');
    $nbrMAxPages = $configTarif->get('nbr_max_pages') ?? 3;
    $fiche = [
      'items' => [
        'editique' => [
          'prix' => 0,
          'bascule' => 0,
          'success' => 0,
          'echec' => 0,
          'docs' => 0,
          'tarif' => [
            'ht' => 0,
            'ttc' => 0
          ],
          'appels' => 0,
          'pages' => 0
        ],
        'convert' => [
          'prix' => 0,
          'bascule' => 0,
          'success' => 0,
          'echec' => 0,
          'docs' => 0,
          'tarif' => [
            'ht' => 0,
            'ttc' => 0
          ],
          'appels' => 0,
          'pages' => 0
        ]
        ],
        'reduce' => $configTarif->get('reduce'),
        'total' => ['ht' => 0, 'ttc' => 0, 'htr' => 0],
        'periode' => $periode,
        'txtva' => $configTarif->get('tva'),
        'tvaamount' => 0,
        'reduceamount' => 0
    ];
    $logs = $this->getLogs(['user_id' => $account->id(), 'periode' => $periode]);
    if (!empty($logs)) {
      foreach($logs as $log) {
        $action = $log->get('field_ws_log_action')->getString();
        if (array_key_exists($action, $fiche['items'])) {
          // Recup des docs et page
          switch ($log->get('field_ws_log_statut')->getString()) {
            case 'success':
              $fiche['items'][$action]['success']++;
              $fiche['items'][$action]['appels']++;
              $models = $log->get('field_ws_log_modeles')->referencedEntities();
              foreach ($models as $model) {
                $nb = intval($model->get('field_ws_modele_nb_pages')->getString());
                $fiche['items'][$action]['pages'] += $nb;
                if ($nb > $nbrMAxPages) {
                  $fiche['items'][$action]['docs'] +=  ($nb / $nbrMAxPages);
                }else {
                  $fiche['items'][$action]['docs']++;
                }
              }
              break;
            case 'warning':
            case 'error':
              $fiche['items'][$action]['echec']++;
              break;
            default:
              break;
          }
        }
      }
      // Calcul du Tarif
      foreach ($fiche['items'] as $k => &$f) {
        $price = $configTarif->get($k . '_price');
        $bascule = $configTarif->get($k . '_limit_bascule_reduce');
        $tarif = $tarifr = $f['docs'] * floatval($price);
        if ($f['docs'] > $bascule) {
          $tarifr = $tarif - ($tarif * floatval($fiche['reduce'])) / 100;
          $fiche['reduceamount'] += ($tarif - $tarifr);
        }
        $f['tarif'] = [
          'htr' => $tarifr,
          'ht' => $tarif,
          'ttc' => $tarifr + ($tarifr * floatval($fiche['txtva'])) / 100,
        ];
        // Total
        $fiche['total']['ht'] += $tarif;                // Total HT sans réduction
        $fiche['total']['ttc'] += $f['tarif']['ttc'];   // Total TTC réduction (avec réduction)
        $fiche['total']['htr'] += $tarifr;              // Total HT avec réduction
        $fiche['tvaamount'] += ($f['tarif']['ttc'] - $tarifr);

        // Affection du prix et du vole vascule
        if ($f['docs'] > 0) {
          $f['prix'] = $price;
          $f['bascule'] = $bascule;
        }
      }
    }

    return $fiche;
  }
  /**
   * @param String $action
   * @return mixed|null
   */
  public function getServiceByAction(String $action) {
    $servicesTabs = [];
    $nodes = $this->getServicesEntities();
    if (!empty($nodes)) {
      foreach ($nodes as $node) {
        switch ($node->get('field_cri_paragraph_titre')->getString()) {
          case 'Editique':
            $servicesTabs['editique'] = $node->id();
            break;
          case 'Edition docx':
            $servicesTabs['docx'] = $node->id();
            break;
          case 'APIsation':
            $servicesTabs['api'] = $node->id();
            break;
          case 'Tableau':
            $servicesTabs['tableau'] = $node->id();
            break;
          case 'Conversion':
            $servicesTabs['convert'] = $node->id();
            break;
        }
      }
    }
    return !empty($servicesTabs[$action]) ? $servicesTabs[$action] : NULL;
  }

  /**
   * @return array
   */
  public function getServicesEntities() {
    $servicesIds = Drupal::entityQuery('node')
      ->accessCheck(FALSE)
      ->condition('type', 'services')
      ->condition('title', 'Notre offre de services')
      ->execute();
    $servicesTabs = [];
    if (!empty($servicesIds)) {
      $serviceId = reset($servicesIds);
      $node = Node::load($serviceId);
      if (!empty($node)) {
        if (!empty($node)) {
          $serviceList = $node->get('field_cri_service_liste')->referencedEntities();
          if (!empty($serviceList)) {
            foreach ($serviceList as $list) {
              $servicesTabs[$list->id()] = $list;
            }
          }
        }
      }
    }

    return $servicesTabs;
  }

  /**
   * @param $service_id
   * @return array|Drupal\Core\Entity\EntityBase[]|Drupal\Core\Entity\EntityInterface[]
   */
  public function getLogs($options = []) {
    $q = Drupal::entityQuery('node')
      ->accessCheck(FALSE)
      ->condition('type', 'ws_logger');
    if (!empty($options['user_id'])) {
      $q->condition('field_ws_log_logger', $options['user_id']);
    }
    if (!empty($options['service_id'])) {
      $q->condition('field_ws_log_service', $options['service_id']);
    }
    if (!empty($options['periode'])) {
      $periode = $options['periode'];
      $q->condition('created', $periode['start']->getTimestamp(), '>=')
        ->condition('created', $periode['end']->getTimestamp(), '<=');
    }
    $ids = $q->execute();

    if (!empty($ids)) {
      return Node::loadMultiple($ids);
    }

    return [];
  }
  /**
   * @param $request
   * @return array
   */
  private function createModels(&$request) {
    $models = [];
    $globalStatus = 0;
    if (!empty($request->models)) {
      foreach ($request->models as $model) {
        $statut = $model->codeError ? 'error' : 'success';
        $globalStatus += $model->codeError;
        $modeleEntity = Node::create([
          'title' => $this->t('Document modèle'),
          'type' => 'ws_editique_modele',
          'field_ws_modele_cle_document' => $model->docKey,
          'field_ws_modele_convertisseur' => $model->convertor,
          'field_ws_modele_format' => $model->format,
          'field_ws_modele_nb_pages' => $model->pages,
          'field_ws_modele_document' => !empty($model->file) ? $model->file : NULL,
          'field_ws_modele_code_erreur' => $model->codeError,
          'field_ws_modele_message_erreur' => $model->msgError,
          'field_ws_modele_statut' => $statut
        ]);
        try {
          $modeleEntity->save();
          $models[] = $modeleEntity->id();
        } catch (\Exception $exc) {
          error_log($exc->getMessage());
        }
      }
    }
    return [$this->getErrorKey($globalStatus, $request), $models];
  }

  /**
   * @param $file
   * @return false|int
   */
  public function pagesCount($file) {
    if (file_exists($file)) {
      $infos = pathinfo($file);
      if (in_array($infos['extension'], ['pdf'])) {
        if (FALSE !== ($content = file_get_contents($file))) {
          $nb = preg_match_all("/\/Page\W/", $content, $matches);
          return $nb;
        }
      }
    }
    return 0;
  }

  /**
   * @param $errorCode
   * @param $request
   * @return string
   */
  private function getErrorKey($errorCode, $request) {
    if ($errorCode === 0) {
      return 'success';
    }

    $nbModels = count($request->models);
    if ($errorCode < $nbModels) {
      return 'warning';
    }
    return 'error';
  }

  /**
   * @param $file
   * @return int
   */
  public function pagesCountDocx($file) {
    $zip = new ZipArchive();
    if (true === $zip->open($file)) {
      $data = $zip->getFromName('docProps/app.xml');
      $zip->close();
      if ($data) {
        $xml = new \DOMDocument();
        $xml->loadXML($data);
        $pageTags = $xml->getElementsByTagName('Pages');
        return !empty($pageTags) ? $pageTags->item(0)->nodeValue : 0;
      }
    }
    return 0;
  }

  /**
   * @param $subscribe
   * @return \DateTime[]
   * @throws \Exception
   */
  public function getPeriode($subscribe) {
    $createDate = intval($subscribe->get('created')->getString());
    $day = date('d', $createDate);
    $nDay = date('d');
    if ($nDay >= $day) {
      $now = date('Y-m-') . $day;
    }else {
      $start =  new \DateTime(date('Y-m-d') . ' -1 month');
      $now = $start->format('Y-m-') . $day;
    }
    $start = new \DateTime($now);
    $end = new \DateTime($now . '+1 month -1 day');
    return [
      'start' => $start,
      'end' => $end
    ];
  }
  /**
   * @param $subscribe
   * @return \DateTime[]
   * @throws \Exception
   */
  public function getPeriodes($subscribe) {
    $createDate = intval($subscribe->get('created')->getString());
    $start = $this->drupalDate(date('Y-m-d', $createDate));
    $day = date('d', $createDate);
    $now = $this->drupalDate(date('Y-m-d'));
    $periodes = [];
    while($start->format('Ymd') < $now->format('Ymd')) {
      $end = $this->drupalDate($start->format('Y-m-') . $day . ' -1 day +1 month');
      // dump(array($start->format('Ymd'), $end->format('Ymd'), $now->format('Ymd'), ($end->format('Ymd') < $now->format('Ymd'))));
      if ($end->format('Ymd') < $now->format('Ymd')) {
        $periodes[] = [
          'start' => $start,
          'end' => $end
        ];
      }
      $start = $this->drupalDate($end->format('Y-m-') . $day);
    }

    return $periodes;
  }
  /**
   * Return a Drupal date formated
   */
  public function drupalDate($stringDate) {
    $date = new DrupalDateTime($stringDate);
    $date->setTimezone(new \DateTimeZone(DateTimeItemInterface::STORAGE_TIMEZONE));
    return $date;
  }
}
