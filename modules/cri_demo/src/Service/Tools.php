<?php

namespace Drupal\cri_demo\Service;

use Drupal;
use Drupal\Core\File\FileSystemInterface;
use Drupal\file\Entity\File;
use Drupal\node\NodeInterface;

class Tools
{
  /**
   * Send ajax request.
   *
   * @param array $data
   * @param NodeInterface $setting
   * @return array
   * @throws Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws Drupal\Core\Entity\EntityStorageException
   */
  public function submitAjaxRequest($data = [], NodeInterface $setting)
  {
    global $base_url;

    $model_file_default = $this->getModelFile();
    if (!empty($setting->get('field_settings_template')->getString())) {
      $models = $setting->get('field_settings_template')->referencedEntities();
      if (!empty($models)) {
        $model_file_default = reset($models);
      }
    }

    if (!empty($model_file_default)) {
      foreach ($data['call']['models'] as $key => $m) {
        $data['call']['models'][$key]['format'] = $setting->get('field_settings_output_format')->getString();
        $data['call']['models'][$key]['contentFile'] = base64_encode(file_get_contents($model_file_default->getFileUri()));
      }
    }

    $_remote = !empty($setting->get('field_settings_remote')->getString()) ? TRUE : FALSE;
    $result = Drupal::service('default.rest.editics.service')->_call($data, ['_remote' => $_remote, '_env' => 'test', '_type' => 'demo']);

    // Traitement pour ffichage du document.
    $response = [
      'result' => $result
    ];

    if (empty($result[0]['errorCode'])) {
      $doc = reset($result);
      if (empty($doc['file']['content'])) {
        $uri = 'public://' . basename($doc['file']);
      } elseif(!empty($doc['file']['content'])) {
        $uri = 'public://' . uniqid() . '_model.' . $doc['file']['extension'];
        file_put_contents($uri, base64_decode($doc['file']['content']));
        $doc['file'] = Drupal::service('file_url_generator')->generateAbsoluteString($uri);
      }

      $pathInfos = pathinfo($uri);
      if ($pathInfos['extension'] == 'docx') {
        $file = File::create([
          'type' => 'file',
          'uri' => $uri,
          'filename' => basename($uri)
        ]);
        $setting->set('field_settings_view_document', $file);

        // view
        $view = Drupal::entityTypeManager()->getViewBuilder('node')->view($setting, 'json_response');
        $response['view'] = Drupal::service('renderer')->render($view);
      }

      // PDF Embed API

      if ($pathInfos['extension'] == 'pdf') {
        $response['api'] = [
          'uri' =>  $doc['file'],
          'fileName' => basename($uri),
          'clientId' => Drupal::service('settings')->get('adobe_api_key')
        ];
      }
    } else {
      $response['view'] = '<div class="loading"><div class="loading-content"><div class="loader"></div><p>Loading error..</p></div></div>';
    }

    return $response;
  }

  /**
   * Recup default model.
   *
   * @return Drupal\Core\Entity\ContentEntityBase|Drupal\Core\Entity\EntityBase|Drupal\Core\Entity\EntityInterface|File|false|mixed|null
   * @throws Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws Drupal\Core\Entity\EntityStorageException
   */
  public function getModelFile($setting = NULL): File | NULL {
    $directory = 'public://assets/files/demo';
    if (\Drupal::service('file_system')->prepareDirectory($directory, FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS)) {
      $destination = $directory . '/demo.docx';
    }

    if (!empty($setting) && !empty($setting->get('field_settings_template')->getString())) {
      $models = $setting->get('field_settings_template')->referencedEntities();
      if (!empty($models)) {
        return reset($models);
      }
    }

    $modelFile = Drupal::service('extension.list.module')->getPath('cri_demo') . '/src/Resources/demo.docx';
    if (file_exists($modelFile)) {
      // remove file if exists
      if (file_exists($destination)) unlink($destination);

      copy($modelFile, $destination);
      $file = File::create([
        'filename' => t('Demonstration'),
        'uri' => $destination
      ]);
      $file->save();
      chmod(Drupal::service('file_system')->realpath($destination), 0777);

      return $file;
    }

    return NULL;
  }
}
