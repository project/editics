<?php

namespace Drupal\cri_demo\Controller;

use Drupal;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Controller\ControllerBase;
use Drupal\node\Entity\Node;

class DemoController extends ControllerBase
{
  /**
   * Open demo page.
   *
   * @return array
   * @throws Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function page() {
    // Drupal::service('session')->remove('demo.entity');
    $settingEntity = Drupal::service('session')->get('demo.entity');

    if (empty($settingEntity)) {
      $settingEntities = Drupal::entityTypeManager()->getStorage('node')->loadByProperties(['type' => 'demo_settings', 'status' => 1]);
      if (!empty($settingEntities)) {
        $settingEntity = reset($settingEntities);
      } else {
        $settingEntity = Node::create([
          'type' => 'demo_settings',
          'title' => t('Demo settings'),
          'status' => TRUE,
          'field_settings_output_format' => 'docx',
          'field_settings_flow_mode' => 'ws_mono_doc'
        ]);
        Drupal::service('session')->set('demo.entity', $settingEntity);
      }
    }

    $form['settings'] = $this->entityFormBuilder()->getForm($settingEntity, 'default');
    $form['flow'] = $this->entityFormBuilder()->getForm($settingEntity, 'json_request');

    $settingClone = clone $settingEntity;
    if ($settingClone) {
    // Model
      $model = Drupal::service('cri_demo.tools.service')->getModelFile($settingEntity);
      if($model) {
        $settingClone->set('field_settings_template', $model->id());
      }

    // view
      $view = Drupal::entityTypeManager()->getViewBuilder('node')->view($settingClone, 'modele_editique');
      $form['model'] = Drupal::service('renderer')->render($view);

    // Suppression de l'entité clonée
      $settingClone->delete();
    }

    return [
      '#theme' => 'demo_main_theme',
      '#data' => [
        'form' => $form
      ],
      '#attached' => [
        'library' => ['cri_demo/cri_jsoneditor'],
        ]
    ];
  }

  /**
   * Call api via jquery.
   *
   * @return Drupal\Core\Ajax\AjaxResponse
   */
  public function api() {
    $json_data = Drupal::request()->get('json_data');;
    $data = Json::decode($json_data);

    $settingEntity = Drupal::service('session')->get('demo.entity');
    $response = Drupal::service('cri_demo.tools.service')->submitAjaxRequest($data, $settingEntity);

    return new Drupal\Core\Ajax\AjaxResponse($response);
  }
}
