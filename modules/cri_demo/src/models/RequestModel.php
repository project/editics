<?php


namespace Drupal\cri_demo\models;


class RequestModel
{
  /**
   * @var string
   */
  public $action = '';
  /**
   * @var bool
   *
   */
  public $download = true;
  /**
   * @var \Drupal\cri_demo\models\ModelModel
   */
  public $models = [];
}
