<?php

namespace Drupal\cri_demo\models;

class ModelModel
{
  /**
   * @var string
   */
  public $docKey = '';
  /**
   * @var string
   */
  public $format = 'pdf';
  /**
   * @var string
   */
  public $convertor = 'soffice';
  /**
   * @var string
   */
  public $contentFile = '';
  /**
   * @var \Drupal\cri_demo\models\Data
   */
  public $settings;
}
