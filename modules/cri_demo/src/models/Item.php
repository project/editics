<?php

namespace Drupal\cri_demo\models;

class Item
{
  /**
   * @var string
   */
  public $id = '';
  /**
   * @var string
   */
  public $type = '';
  /**
   * @var string
   */
  public $value = '';
}
