
(function ($, Drupal) {
  Drupal.behaviors.custom_jsoneditor = {
    attach: function (context) {
      $('.jsoneditor--wrapper').each(function () {
        if(context != document && context.nodeName != 'FORM') {
          return;
        }

        var options = {
          mode: 'tree',
          modes: ['code', 'tree', 'view', 'form', 'text'],
          ace: ace,
          container: this,
          onChangeText: function (jsonString) {
            var input = getInputFromContainer(this.container);
            if (input) {
              input.val(jsonString);
            }

            // Call api : désactivé pour éviter l'appel en plusieurs séquences pour chaque modification dans l'éditeur.
            // Seul l'action du bouton [Appliquer] appelera le Viewer.
            // $.post('/admin/demo/api', {json_data: jsonString}, function (response) {
            //   $.fn.updateJsonResponse('ws_response', response);
            // });
          }
        };

        if (!this.editor) {
          this.editor = new JSONEditor(this, options);
        }
        var input = getInputFromContainer(this);

        if (input) {
          var inputValue = input.val();

          if (inputValue) {
            var json = JSON.parse(inputValue);
            this.editor.set(json);
          } else {
            this.editor.set([]);
          }
        }
      });

      // Si Bouton save cliqué, fermeture du panel des options
      $('#edit-ajax').on('click', function(ev) {
        $.fn.editOpen();
      });
    },
  };

  function getInputFromContainer(container) {
    var inputName = $(container).data('input');

    if (inputName) {
      var input = $('input[name="' + inputName + '"]');

      if (input.length > 0) {
        return input;
      }
    }

    return null;
  }

  $.fn.updateJsonResponse = function (inputName, response) {

    // Hide custol lodaer.
    $('.loading').each(function (k, item) { item.style.display = 'none'; });

    // Document viewer - Microsoft Document.
    if (response.view) {
      $('#panel-document-view').html(response.view);
    }

    // Document viewer - Adobe : PDF Embed API
    if (response.api) {
      var adobeDCView = new AdobeDC.View({ clientId: response.api.clientId, divId: "panel-document-view" });
      adobeDCView.previewFile({
        content:{ location: { url: response.api.uri }},
        metaData:{ fileName: response.api.fileName }
      }, { defaultViewMode: "FIT_WIDTH" });
    }

    // Json Response
    var editorDiv = $('div.jsoneditor--wrapper[data-input="' + inputName + '"]:first');
    if (editorDiv.length > 0) {
      editorDiv[0].editor.set(response.result);
    }
  }

})(jQuery, Drupal);
