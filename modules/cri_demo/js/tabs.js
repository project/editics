
(function ($, Drupal) {
  Drupal.behaviors.cri_jsoneditor = {
    attach: function (context) {
      $('.tablinks').on('click', function (evt) {
        openTab(evt, this);
      });

    },
  };

  function openTab(evt, me) {
    var i, tabcontent, tablinks;
    tabcontent = $('.tabcontent');
    for (i = 0; i < tabcontent.length; i++) {
      tabcontent[i].style.display = "none";
    }
    tablinks = $('.tablinks');
    for (i = 0; i < tablinks.length; i++) {
      tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(me.id + '-content').style.display = "block";
    evt.currentTarget.className += " active";
  }

})(jQuery, Drupal);
