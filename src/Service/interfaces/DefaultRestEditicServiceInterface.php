<?php

namespace Drupal\editics\Service\interfaces;

/**
 * Interface DefaultRestEditicServiceInterface.
 */
interface DefaultRestEditicServiceInterface {
  /**
   * @param $request
   * @return mixed
   */
  public function process($request);
}
