<?php

namespace Drupal\editics\Service\interfaces;

use Drupal\Core\Session\AccountProxyInterface;

/**
 * Interface DefaultLoggerServiceInterface
 */
interface DefaultLoggerServiceInterface {
  /**
   * @param AccountProxyInterface $user
   * @param $request
   */
  function logger(AccountProxyInterface $user, &$request);

}
