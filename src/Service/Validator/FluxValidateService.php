<?php


namespace Drupal\editics\Service\Validator;

use Drupal;
use Drupal\Core\Url;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;

class FluxValidateService {

  /**
   * Injection de la fonction de traduction.
   */
  use StringTranslationTrait;

  /**
   * __construct
   *
   * @param  mixed $stringTranslation
   * @return void
   */
  public function __construct(TranslationInterface $stringTranslation)
  {

  }

  /**
   * @param $request
   * @return bool
   */
  protected function validate($request) {
    // Validate Models key
    if (empty($request->models)) {
      $this->defaultRestErrorService->add('rest_key_models', 'Models key is empty!');
      return FALSE;
    }
    foreach ($request->models as $k => $model) {
      // Validate format
      if (empty($model->format)) {
        $this->defaultRestErrorService->add('rest_key_format', 'format of ' . $model->docKey . ' model is empty!');
        return FALSE;
      }
      // Validate convertor
      if ($model->format === 'pdf') {
        if (empty($model->convertor)) {
          $this->defaultRestErrorService->add('rest_key_doc_key', 'convertor of model ' . ($k + 1) . ' is empty!');
          return FALSE;
        }
      }
      // Validate convertor value
      if ($model->format === 'pdf') {
        if (!in_array($model->convertor, ['soffice', 'adobe', 'api', 'local', 'dompdf', 'mpdf'])) {
          $this->defaultRestErrorService->add('rest_key_doc_key', 'convertor of model ' . ($k + 1) . ' must check once soffice|adobe|api|local!');
          return FALSE;
        }
      }
      // Validate docKey
      if (empty($model->docKey)) {
        $this->defaultRestErrorService->add('rest_key_doc_key', 'docKey of model ' . ($k + 1). ' is empty!');
        return FALSE;
      }
      // Validate docKey
      if (preg_match("/[\s+!àéè@#$%^&*()+-=\[\]{};':\",.<>\/]/i", $model->docKey, $matches)) {
        $this->defaultRestErrorService->add('rest_key_doc_key', 'docKey of model ' . ($k + 1). ' has space or special character [' . $matches[0] . ']!');
        return FALSE;
      }
      // Validate content file
      if (empty($model->contentFile)) {
        $this->defaultRestErrorService->add('rest_key_format', 'contentFile key of ' . $model->docKey . ' model is empty!');
        return FALSE;
      }
    }
    return TRUE;
  }

  /**
   * @param $content
   * @return array|mixed
   */
  public function remote($content, $options) {
    $_env = isset($options['_env']) ? $options['_env'] : 'test';
    $credentials = Drupal::config('editics.api.settings');

    switch($_env) {
      case 'production':
      case 'prod':
        $url = $credentials->get('global_prod_url_server_access');
        $credential_id = $credentials->get('global_prod_credential_id');
        $credential_pass = $credentials->get('global_prod_credential_pass');
        break;

      default:
        $url = $credentials->get('global_recette_url_server_access');
        $credential_id = $credentials->get('global_recette_credential_id');
        $credential_pass = $credentials->get('global_recette_credential_pass');
        break;
    }


    if (empty($url) || empty($credential_id) || empty($credential_pass)) {
      return [
        [
          'errorCode' => 1,
          'message' => $this->t('Login data is empty, go to the setup page : @url', [
            '@url' => Url::fromRoute('editics.configuration.api')->setAbsolute()->toString()
          ])
        ]
      ];
    }
    $httpClient = Drupal::httpClient();
    $response = $httpClient->post($url, [
      'json' => $content,
      'verify' => false,
      'headers' => [
        'Authorization' => 'Basic ' . base64_encode($credential_id . ':' . $credential_pass),
        'Content-Type' => 'application/json',
      ]
    ]);
    $result = $response->getBody()->getContents();
    if (!empty($result)) {
      return json_decode($result, true);
    }
    return [];
  }
}
