<?php

namespace Drupal\editics\Service;

use Drupal;
use Drupal\Component\Serialization\Json;
use Drupal\editics\Service\Validator\FluxValidateService;
use Drupal\cri_php_word\Service\error\DefaultRestErrorServiceInterface;
use Drupal\cri_php_word\Service\DefaultEditorService;
use Drupal\editics\Service\interfaces\DefaultRestEditicServiceInterface;
use Drupal\editics\Service\interfaces\DefaultLoggerServiceInterface;
use Drupal\node\Entity\Node;
use Drupal\paragraphs\Entity\Paragraph;
/**
 * Class DefaultRestEditicService.
 */
class DefaultRestEditicService extends FluxValidateService implements DefaultRestEditicServiceInterface {

  /**
   * Drupal\editics\Service\error\DefaultRestErrorServiceInterface definition.
   *
   * @var DefaultRestErrorServiceInterface
   */
  protected DefaultRestErrorServiceInterface $defaultRestErrorService;

  /**
   * Drupal\cri_php_word\Service\DefaultEditorService definition.
   *
   * @var DefaultEditorService
   */
  protected DefaultEditorService $defaultEditorService;
  /**
   * @var DefaultLoggerServiceInterface
   */
  protected DefaultLoggerServiceInterface $defaultLoggerService;

  /**
   * Constructs a new DefaultRestEditicService object.
   * @param DefaultRestErrorServiceInterface $default_rest_error_service
   * @param DefaultEditorService $default_editor_service
   * @param DefaultLoggerServiceInterface $default_logger_service
   */
  public function __construct(DefaultRestErrorServiceInterface $default_rest_error_service, DefaultEditorService $default_editor_service, DefaultLoggerServiceInterface $default_logger_service) {
    $this->defaultRestErrorService = $default_rest_error_service;
    $this->defaultEditorService = $default_editor_service;
    $this->defaultLoggerService = $default_logger_service;
  }

  /**
   * @param $request
   * @return array|false|mixed|string
   */
  public function process($request)
  {
    if ($this->validate($request)) {
      return $this->defaultEditorService->edit($request);
    }
    return [
      [
        'errorCode' => 1,
        'message' => $this->defaultRestErrorService->get()
      ]
    ];
  }

  /**
   * @param $request
   * @return bool
   */
  protected function validate($request) {
    $validate = parent::validate($request);
    if ($validate) {
      foreach ($request->models as $k => $model) {
        // Validate settings
        if (empty($model->settings)) {
          $this->defaultRestErrorService->add('rest_key_format', 'settings key of ' . $model->docKey . ' model is empty!');
          return FALSE;
        }
      }
    }
    return $validate;
  }
  /**
   * @param $content
   * @return array|string
   */
  public function _call($content, $options) {
    $_remote = isset($options['_remote']) ? $options['_remote'] : false;
    $_type = isset($options['_type']) ? $options['_type'] : NULL;
    $content['call']['remote'] = $_remote;
    $start = microtime(true);

    try {
      if (!empty($_remote)) {
        $response = $this->remote($content, $options);
      }else {
        $response = $this->process(json_decode(json_encode($content['call'])));
      }
      $end = microtime(true);
      $response['delay'] = number_format(($end - $start), 3) . ' sec';

      foreach ($response as $key => &$res) {
        switch ($key) {
          case 'delay':
          case 'code':
          case 'message':
            break;
          default:
            if (!empty($res['file']['content'])) {
              $body = base64_decode($res['file']['content']);
              if (empty($_type) || $_type !== 'demo') {
                $res['file']['content'] = $body;
              }
              if (preg_match('/word\/document/i', $body)) {
                $res['file']['extension'] = 'docx';
              }
              if (preg_match('/PDF-/i', $body)) {
                $res['file']['extension'] = 'pdf';
              }
              krsort($res['file']);
            }
            break;
        }
      }

      return $response;

    } catch (\Exception $exc) {
      $end = microtime(true);
      return [
        'delay' => ($end - $start),
        'errorCode' => $exc->getCode(),
        'message' => json_decode(json_encode($exc->getMessage(), true))
      ];
    }
  }

}
