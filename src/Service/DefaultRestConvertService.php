<?php

namespace Drupal\editics\Service;

use Drupal\editics\Service\interfaces\DefaultLoggerServiceInterface;
use Drupal\editics\Service\interfaces\DefaultRestEditicServiceInterface;
use Drupal\editics\Service\Validator\FluxValidateService;
use Drupal\cri_php_word\Service\DefaultEditorService;
use Drupal\cri_php_word\Service\error\DefaultRestErrorServiceInterface;

/**
 * Class DefaultRestConvertService.
 */
class DefaultRestConvertService extends FluxValidateService implements DefaultRestEditicServiceInterface {

  /**
   * Drupal\editics\Service\error\DefaultRestErrorServiceInterface definition.
   *
   * @var DefaultRestErrorServiceInterface
   */
  protected DefaultRestErrorServiceInterface $defaultRestErrorService;

  /**
   * Drupal\cri_php_word\Service\DefaultEditorService definition.
   *
   * @var DefaultEditorService
   */
  protected DefaultEditorService $defaultEditorService;
  /**
   * @var DefaultLoggerServiceInterface
   */
  protected DefaultLoggerServiceInterface $defaultLoggerService;

  /**
   * Constructs a new DefaultRestEditicService object.
   * @param DefaultRestErrorServiceInterface $default_rest_error_service
   * @param DefaultEditorService $default_editor_service
   * @param DefaultLoggerServiceInterface $default_logger_service
   */
  public function __construct(DefaultRestErrorServiceInterface $default_rest_error_service, DefaultEditorService $default_editor_service, DefaultLoggerServiceInterface $default_logger_service) {
    $this->defaultRestErrorService = $default_rest_error_service;
    $this->defaultEditorService = $default_editor_service;
    $this->defaultLoggerService = $default_logger_service;
  }
  /**
   * @param $request
   * @return array|false|mixed|string
   */
  public function process($request)
  {
    if ($this->validate($request)) {
      return $this->defaultEditorService->edit($request);
    }
    return $this->defaultRestErrorService->get();
  }

  /**
   * @param $request
   * @return bool
   */
  protected function validate($request) {
    return parent::validate($request);
  }
}
