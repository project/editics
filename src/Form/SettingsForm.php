<?php

namespace Drupal\editics\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class SettingsForm.
 *
 * @package Drupal\editics\Form
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'editics_api_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'editics.api.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config('editics.api.settings');

    // Paramétrage global.
    $form['global'] = [
      '#type' => 'details',
      '#title' => 'Paramétrages globaux',
      '#open' => true,
      'production' => [
        '#type' => 'fieldset',
        '#title' => $this->t('Production'),
        'global_prod_url_server_access' => [
          '#type' => 'textfield',
          '#title' => $this->t('Url serveur distant de production'),
          '#default_value' => $config->get('global_prod_url_server_access')
        ],
        'global_prod_credential_id' => [
          '#type' => 'textfield',
          '#title' => $this->t('Identifiant client de connexion'),
          '#default_value' => $config->get('global_prod_credential_id'),
        ],
        'global_prod_credential_pass' => [
          '#type' => 'password',
          '#title' => $this->t('Mot de passe de connexion'),
          '#default_value' => $config->get('global_prod_credential_pass'),
        ]
      ],
      'recette' => [
        '#type' => 'fieldset',
        '#title' => $this->t('Recette'),
        'global_recette_url_server_access' => [
          '#type' => 'textfield',
          '#title' => $this->t('Url serveur distant de recette'),
          '#default_value' => $config->get('global_recette_url_server_access')
        ],
        'global_recette_credential_id' => [
          '#type' => 'textfield',
          '#title' => $this->t('Identifiant client de connexion'),
          '#default_value' => $config->get('global_recette_credential_id'),
        ],
        'global_recette_credential_pass' => [
          '#type' => 'password',
          '#title' => $this->t('Mot de passe de connexion'),
          '#default_value' => $config->get('global_recette_credential_pass'),
        ]
      ]
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Sauvegarde de la configuration.
    $config = $this->config('editics.api.settings');
    $values = $form_state->getValues();
    //dump($values); die;
    foreach ($values as $key => $value) {
      $config->set($key, $value);
    }
    $config->save();

    \Drupal::messenger()->addMessage($this->t('Configuration sauvegardée.'));
  }

}
